<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    function add_fonts() {
        // Styles Format:  wp_enqueue_style($handle, $src, $deps, $ver, $media);
            wp_register_style('montserrat', 'https://fonts.googleapis.com/css?family=Montserrat&display=swap', array(), false, 'all');
            wp_enqueue_style( 'montserrat');

            wp_register_style('poppins', 'https://fonts.googleapis.com/css?family=Poppins&display=swap', array(), false, 'all');
            wp_enqueue_style( 'poppins');
    }
    add_action('wp_enqueue_scripts', 'add_fonts', 100);

    function bootstrap() {
        
   
        wp_register_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), NULL, true );
        wp_register_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', false,  NULL, true );
        wp_register_style( 'bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', false, NULL, 'all' );
    
        wp_enqueue_script( 'popper-js' );
        wp_enqueue_script( 'bootstrap-js' );
        wp_enqueue_style( 'bootstrap-css' );
    }
    add_action( 'wp_enqueue_scripts', 'bootstrap', 100 );

    function my_scripts_method() {
        wp_enqueue_script(
            'custom-script',
            get_stylesheet_directory_uri() . '/sitejs.js',
            array( 'jquery' )
        );
    }
    
    add_action( 'wp_enqueue_scripts', 'my_scripts_method',200 );

  
    require_once get_stylesheet_directory()  . '/class-wp-bootstrap-navwalker.php';
    register_nav_menus( array(
        'primary' => __( 'main', 'Nada' ),
    ) );
}

