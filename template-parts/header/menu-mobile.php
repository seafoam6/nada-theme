<div class="d-block d-lg-none title-bar sticky-top nav-mobile">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <a href="#" title="Nada Daher">
          <h1 class="site-title cap fb swish-left">Nada Daher</h1>
        </a>		
        <div class="dropdown ">
          <button class="btn dropdown-toggle hamburger" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span></span>
            <span></span>
            <span></span>

          </button>

          <?php 
		wp_nav_menu( array(
			'theme_location'    => 'menu-1',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'dropdown-menu dropdown-menu-right',
			// 'container_id'      => 'bs-example-navbar-collapse-1',
			// 'menu_class'        => 'nav navbar-nav',
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new WP_Bootstrap_Navwalker(),
		) );
		?>
		

        </div>
      </div>
    </div>
  </div>
</div>