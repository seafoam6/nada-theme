<div class="d-none d-lg-block title-bar sticky-top desktop-menu">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="get-in-line">
		<?php get_template_part( 'template-parts/content/logo', 'text' ); ?>
          <?php 
          wp_nav_menu( array(
            'theme_location'    => 'menu-1',
            'depth'             => 2,
            'container'         => 'nav',
            // 'container_class'   => 'nav',
            'container_id'      => 'spymenu',
            'menu_class'        => 'nav',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker(),
          ) );
        ?>
        </div>  
      </div>
    </div>
  </div>
</div>